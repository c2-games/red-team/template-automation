# Rocky 9 Template

This is how the NCAE Red Team builds the Rocky 9 images used during competition.

## Running Rocky 9 Packer Build

Prior to running the build, an environment file need to be filled out.
The provided `sample-environment` file can be used as a base template for this.

```bash
packer build -var-file=${sample-environment file} .
```

## Variable Overview

This section will explain variables presented in `sample-environment`.
There are further variables available, checkout the `vars.pkr.hcl` for a full list.

### Proxmox variables

- `proxmox_hostname`
  - Points to the base url of proxmox
- `proxmox_username`
  - Username/API name used to authenticate to Proxmox
- `proxmox_password`
  - Token for provided `proxmox_username`
- `proxmox_node_name`
  - Node within cluster template will be created on
- `proxmox_insecure_skip_tls_verify`
  - If TLS verify should be skipped
- `template_description`
  - Description given to the created template
  - A date generated notice will always appear first within the description

### Template variables
- `vm_id`
  - VM/Template ID given to create template
- `vm_pool`
  - Pool template is created in
- `vm_tags`
  - List of tags given to the template
  - This is list `;` separated (example: `tag1;tag2;tag3`)
- `vm_name`
  - Name of created template
- `vm_storage_pool`
  - Storage pool template is stored within
- `vm_cloud_init_storage_pool`
  - Storage pool cloud-init drive is stored within
- `vm_cores`
  - Number of cores given to template
- `vm_memory`
  - Amount of RAM on bytes given to template
- `vm_sockets`
  - Number of CPU sockets presented to template
- `vm_cpu_type`
  - CPU type Proxmox option
- `vm_os`
  - OS Proxmox options
- `vm_network_interface`
  - Network interface given to template
- `vm_scsi_controller`
  - SCSI controller Proxmox option
- `iso_file`
  - Path to ISO file stored within Proxmox
  - Example: `proxmox:iso/path_to_iso.iso`

### VM variables

- `ks_url`
  - URL to raw ks file which is used to define the base template build
- `vm_username`
  - Username provisioned within template
- `path_to_ssh_connect_key`
  - Local path to SSH key used to connect back during build
  - The public key pair to this key should be inserted within the ks file

