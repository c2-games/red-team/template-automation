#############################################################
# Proxmox variables
#############################################################
variable "proxmox_hostname" {
  description = "Proxmox host address (e.g. https://192.168.1.1:8006)"
  type = string
}

variable "proxmox_username" {
  description = "Proxmox username (e.g. root@pam)"
  type = string
}

variable "proxmox_password" {
  description = "Proxmox password"
  type = string
  sensitive = true
}

variable "proxmox_node_name" {
  description = "Proxmox node name"
  type = string
}

variable "proxmox_insecure_skip_tls_verify" {
  description = "Skip TLS verification?"
  type = bool
  default = false
}

#############################################################
# Template variables
#############################################################
variable "template_description" {
  description = "Template description"
  type = string
}

variable "vm_id" {
  description = "VM template ID"
  type = number
}

variable "vm_name" {
  description = "VM name"
  type = string
  default = "kali-template"
}

variable "vm_storage_pool" {
  description = "Storage where template will be stored"
  type = string
}

variable "vm_disk_type" {
  description = "VM disk type"
  type = string
  default = "virtio"
}

variable "vm_disk_size" {
  description = "VM disk size"
  type = string
  default = "8G"
}

variable "vm_disk_format" {
  description = "Storage format given to VM"
  type = string
  default = "qcow2"
}

variable "vm_disk_cache_mode" {
  description = "Disk cache mode"
  type = string
  default = "writeback"
}

variable "vm_disk_io_thread" {
  description = "Disk IO thread"
  type = bool
  default = true
}

variable "vm_disk_discard" {
  description = "Disk discard"
  type = bool
  default = true
}

variable "vm_cores" {
  description = "VM amount of memory"
  type = number
  default = 2
}

variable "vm_memory" {
  description = "VM amount of memory"
  type = number
  default = 2048
}

variable "vm_sockets" {
  description = "VM amount of CPU sockets"
  type = number
  default = 1
}

variable "vm_cpu_type" {
  description = "CPU type to emulate"
  type = string
  default = "host"
}

variable "vm_os" {
  description = "VM operating system"
  type = string
  default = "l26"
}

variable "vm_scsi_controller" {
  description = "SCSI controller model to emulate"
  type = string
  default = "virtio-scsi-single"
}

variable "iso_file" {
  description = "Location of ISO file on the server. E.g. local:iso/<filename>.iso"
  type = string
}

variable "vm_pool" {
  description = "Name of resource pool to create virtual machine in"
  type = string
}

variable "vm_tags" {
  description = "Proxmox UI tags (e.g. tag1;tag2)"
  type = string
}

variable "vm_network_model" {
  description = "VM model of virtual network adapter"
  type = string
  default = "virtio"
}

variable "vm_network_interface" {
  description = "Name of network interface"
  type = string
}

variable "vm_ga_enable" {
  description = "Guest agent enable toggle"
  type = bool
  default = true
}

variable "vm_unmount_iso" {
  description = "If ISO should be unmounted at template creation"
  type = bool
  default = true
}

variable "vm_on_boot" {
  description = "If template should contain onboot flag"
  type = bool
  default = false
}

variable "vm_cloud_init_enable" {
  description = "VM cloud-init toggle"
  type = bool
  default = true
}

variable "vm_cloud_init_storage_pool" {
  description = "VM cloud-init storage pool"
  type = string
}

variable "vm_boot_parm" {
  description = "Override default boot order"
  type = string
  default = "order=virtio0;ide2"
}

#############################################################
# OS Settings
#############################################################
variable "ks_url" {
  description = "URL to ks file"
  type = string
}

variable "vm_username" {
  description = "Default username"
  type = string
}

variable "path_to_ssh_connect_key" {
  description = "Path to staging key -- assumes vm_username is setup with pub key in ks"
  type = string
}

#############################################################
# Timeout Settings
#############################################################
variable "timeout_job" {
  description = "Time until the full job timesout"
  type = string
  default = "1h"
}

variable "timeout_ssh_connect" {
  description = "Time until waiting for ssh connection timesout"
  type = string
  default = "30m"
}

variable "timeout_boot_wait" {
  description = "Time to wait until boot commands are issued"
  type = string
  default = "5s"
}
