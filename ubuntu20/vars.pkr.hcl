#############################################################
# Proxmox variables
#############################################################
variable "proxmox_hostname" {
  description = "Proxmox host address (e.g. https://192.168.1.1:8006)"
  type = string
}

variable "proxmox_username" {
  description = "Proxmox username (e.g. root@pam)"
  type = string
}

variable "proxmox_password" {
  description = "Proxmox password"
  type = string
  sensitive = true
}

variable "proxmox_node_name" {
  description = "Proxmox node name"
  type = string
}

variable "proxmox_insecure_skip_tls_verify" {
  description = "Skip TLS verification?"
  type = bool
  default = false
}

#############################################################
# Template variables
#############################################################
variable "template_description" {
  description = "Template description"
  type = string
}

variable "vm_id" {
  description = "VM template ID"
  type = number
}

variable "vm_name" {
  description = "VM name"
  type = string
  default = "kali-template"
}

variable "vm_storage_pool" {
  description = "Storage where template will be stored"
  type = string
}

variable "vm_disk_format" {
  description = "Disk format type"
  type = string
  default = "qcow2"
}

variable "vm_disk_cache_mode" {
  description = "Disk cache mode"
  type = string
  default = "writeback"
}

variable "vm_disk_io_thread" {
  description = "Disk IO thread"
  type = bool
  default = true
}

variable "vm_disk_discard" {
  description = "Disk discard"
  type = bool
  default = true
}

variable "vm_cores" {
  description = "VM amount of memory"
  type = number
  default = 2
}

variable "vm_memory" {
  description = "VM amount of memory"
  type = number
  default = 2048
}

variable "vm_sockets" {
  description = "VM amount of CPU sockets"
  type = number
  default = 1
}

variable "vm_cpu_type" {
  description = "CPU type to emulate"
  type = string
  default = "host"
}

variable "vm_os" {
  description = ""
  type = string
  default = "l26"
}

variable "vm_scsi_controller" {
  description = ""
  type = string
  default = "virtio-scsi-single"
}

variable "iso_file" {
  description = "Location of ISO file on the server. E.g. local:iso/<filename>.iso"
  type = string
}

variable "vm_cloud_init_enable" {
  description = "VM cloud-init toggle"
  type = bool
  default = true
}

variable "vm_cloud_init_storage_pool" {
  description = "VM cloud-init storage pool"
  type = string
}

variable "vm_pool" {
  description = "Name of resource pool to create virtual machine in"
  type = string
}

variable "vm_tags" {
  description = "Proxmox UI tags (e.g. tag1;tag2)"
  type = string
}

variable "vm_network_interface" {
  description = "Name of network interface"
  type = string
}

#############################################################
# OS Settings
#############################################################
variable "cloudinit_url" {
  description = "URL to cloud-init file"
  type = string
}

variable "vm_username" {
  description = "Default username"
  type = string
}

variable "vm_user_password" {
  description = "Default user's password"
  type = string
  sensitive = true
}

variable "path_to_ssh_connect_key" {
  description = ""
  type = string
}

variable "vm_main_kali_package" {
  description = "Main kali package to be installed"
  type = string
  default = "kali-linux-large"
}

variable "vm_aux_kali_package" {
  description = "Extra packages installed into the system"
  type = string
  default = ""
}

variable "vm_time_zone" {
  description = "Time Zone"
  type = string
  default = "UTC"
}
