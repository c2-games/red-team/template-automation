locals {
  today = timestamp()
}

source "proxmox-iso" "template" {
  proxmox_url = "${var.proxmox_hostname}/api2/json"
  insecure_skip_tls_verify = var.proxmox_insecure_skip_tls_verify
  username = var.proxmox_username
  token = var.proxmox_password
  node = var.proxmox_node_name

  task_timeout = "1h"

  vm_name = var.vm_name
  vm_id = var.vm_id
  pool = var.vm_pool
  tags = var.vm_tags

  memory = var.vm_memory
  sockets = var.vm_sockets
  cores = var.vm_cores
  cpu_type = var.vm_cpu_type
  os = var.vm_os

  network_adapters {
    model = "virtio"
    bridge = var.vm_network_interface
  }

  qemu_agent = true
  scsi_controller = var.vm_scsi_controller

  disks {
    type = "scsi"
    disk_size = "8G"
    storage_pool = var.vm_storage_pool
    format = var.vm_disk_format
    cache_mode = var.vm_disk_cache_mode
    io_thread = var.vm_disk_io_thread
    discard = var.vm_disk_discard
  }

  ssh_username = var.vm_username
  ssh_private_key_file = var.path_to_ssh_connect_key
  ssh_timeout = "30m"

  iso_file = var.iso_file
  cloud_init = var.vm_cloud_init_enable
  cloud_init_storage_pool = var.vm_cloud_init_storage_pool

  onboot = false

  template_name         = var.vm_name
  template_description  = "Date Generated: \n${local.today}\n\n---\n\n${var.template_description}"
  unmount_iso           = true

  boot_wait             = "5s"

  boot_command = [
   #"<esc><wait>",
   "<enter><enter><f6><esc><wait>",
   "autoinstall ds=nocloud-net;seedfrom=${var.cloudinit_url}",
   "<enter><wait>"
  ]
}

build {
  sources = [
    "source.proxmox-iso.template"
  ]
  provisioner "shell" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get upgrade -y"
    ]
  }
}
