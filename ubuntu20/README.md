# Kali Template

This is how the NCAE Red Team builds the Kali images used during competition.

## Running Kali Packer Build

Prior to running the build, an environment file need to be filled out.
The provided `sample-environment` file can be used as a base template for this.

```bash
packer build -var-file=${sample-environment file} .
```

## Variable Overview

This section will explain variables presented in `sample-environment`

### Proxmox variables

- `proxmox_hostname`
  - Points to the base url of proxmox
- `proxmox_username`
  - Username/API name used to authenticate to Proxmox
- `proxmox_password`
  - Token for provided `proxmox_username`
- `proxmox_node_name`
  - Node within cluster template will be created on
- `proxmox_insecure_skip_tls_verify`
  - If TLS verify should be skipped
- `template_description`
  - Description given to the created template
  - A date generated notice will always appear first within the description

### Template variables
- `vm_id`
  - VM/Template ID given to create template
- `vm_pool`
  - Pool template is created in
- `vm_tags`
  - List of tags given to the template
  - This is list `;` separated (example: `tag1;tag2;tag3`)
- `vm_name`
  - Name of created template
- `vm_storage_pool`
  - Storage pool template is stored within
- `vm_cores`
  - Number of cores given to template
- `vm_memory`
  - Amount of RAM on bytes given to template
- `vm_sockets`
  - Number of CPU sockets presented to template
- `vm_cpu_type`
  - CPU type Proxmox option
- `vm_os`
  - OS Proxmox options
- `vm_network_interface`
  - Network interface given to template
- `vm_scsi_controller`
  - SCSI controller Proxmox option
- `vm_time_zone`
  - Option currently not used
- `iso_file`
  - Path to ISO file stored within Proxmox
  - Example: `proxmox:iso/path_to_iso.iso`

### VM variables

- `pre_seed_url`
  - URL to raw preseed.cfg file which is used to define the base template build
- `path_to_scripts_directory`
  - Local path to scripts directory uploaded into Kali
- `vm_username`
  - Username provisioned within template
- `vm_user_password`
  - Password provisioned to `vm_username`
- `vm_main_kali_package`
  - Main package installed during template creation
  - Defaults to `kali-linux-large`
- `vm_aux_kali_package`
  - Auxiliary packages installed during template creation
  - Packages should be space separated (example: `vim nano`)
- `path_to_ssh_connect_key`
  - Local path to SSH key used to connect back during build
  - The public key pair to this key should be inserted within the preseed file

### VM keys

- `vm_user_pub_key`
  - Public key string imported into template
- `vm_user_priv_b64_key`
  - Base64 encoded private key string imported into template

