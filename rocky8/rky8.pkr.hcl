locals {
  today = timestamp()
}

source "proxmox-iso" "template" {
  proxmox_url = "${var.proxmox_hostname}/api2/json"
  insecure_skip_tls_verify = var.proxmox_insecure_skip_tls_verify
  username = var.proxmox_username
  token = var.proxmox_password
  node = var.proxmox_node_name

  task_timeout = var.timeout_job

  vm_name = var.vm_name
  vm_id = var.vm_id
  pool = var.vm_pool
  tags = var.vm_tags

  memory = var.vm_memory
  sockets = var.vm_sockets
  cores = var.vm_cores
  cpu_type = var.vm_cpu_type
  os = var.vm_os

  network_adapters {
    model = var.vm_network_model
    bridge = var.vm_network_interface
  }

  qemu_agent = var.vm_ga_enable
  scsi_controller = var.vm_scsi_controller

  disks {
    type = var.vm_disk_type
    disk_size = var.vm_disk_size
    storage_pool = var.vm_storage_pool
    format = var.vm_disk_format
    cache_mode = var.vm_disk_cache_mode
    io_thread = var.vm_disk_io_thread
    discard = var.vm_disk_discard
  }

  ssh_username = var.vm_username
  ssh_private_key_file = var.path_to_ssh_connect_key
  ssh_timeout = var.timeout_ssh_connect

  iso_file = var.iso_file
  cloud_init = var.vm_cloud_init_enable
  cloud_init_storage_pool = var.vm_cloud_init_storage_pool

  onboot = var.vm_on_boot
  boot = var.vm_boot_parm

  template_name = var.vm_name
  template_description = "Date Generated: \n${local.today}\n\n---\n\n${var.template_description}"
  unmount_iso = var.vm_unmount_iso

  boot_wait = var.timeout_boot_wait

  boot_command = [
   "<tab> text inst.ks=${var.ks_url}<enter><wait>"
  ]
}

build {
  sources = [
    "source.proxmox-iso.template"
  ]
  provisioner "shell" {
    inline = [
      "sudo dnf update -y",
      "sudo rm -f /etc/ssh/ssh_host*"
    ]
  }
}
