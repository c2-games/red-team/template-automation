locals {
  today = timestamp()
}

source "proxmox-iso" "template" {
  proxmox_url = "${var.proxmox_hostname}/api2/json"
  insecure_skip_tls_verify = var.proxmox_insecure_skip_tls_verify
  username = var.proxmox_username
  token = var.proxmox_password
  node = var.proxmox_node_name

  task_timeout = "1h"

  vm_name = var.vm_name
  vm_id = var.vm_id
  pool = var.vm_pool
  tags = var.vm_tags

  memory = var.vm_memory
  sockets = var.vm_sockets
  cores = var.vm_cores
  cpu_type = var.vm_cpu_type
  os = var.vm_os

  network_adapters {
    model = "virtio"
    bridge = var.vm_network_interface
  }

  qemu_agent = true
  scsi_controller = var.vm_scsi_controller

  disks {
    type = "scsi"
    disk_size = "32G"
    storage_pool = var.vm_storage_pool
    format = var.vm_disk_format
    cache_mode = var.vm_disk_cache_mode
    io_thread = var.vm_disk_io_thread
    discard = var.vm_disk_discard
  }

  ssh_username = var.vm_username
  ssh_private_key_file = var.path_to_ssh_connect_key
  ssh_timeout = "30m"

  iso_file = var.iso_file

  onboot = false

  template_name         = var.vm_name
  template_description  = "Date Generated: \n${local.today}\n\n---\n\n${var.template_description}"
  unmount_iso           = true

  boot_wait             = "5s"

  boot_command = [
   "<esc><wait>",
   "install preseed/url=${var.pre_seed_url} ",
   "debian-installer=en_US auto ",
   "locale=en_US kbd-chooser/method=us ",
   "netcfg/get_hostname=hostname netcfg/get_domain=local fb=false ",
   "debconf/frontend=noninteractive console-setup/ask_detect=false ",
   "console-keymaps-at/keymap=us keyboard-configuration/xkb-keymap=us ",
   ## Static Address Config
   #"netcfg/disable_autoconfig=true ",
   #"netcfg/get_ipaddress=172.10.101.62 ",
   #"netcfg/get_netmask=255.255.255.0 ",
   #"netcfg/get_gateway=172.10.101.1 ",
   #"netcfg/get_nameservers=1.1.1.1 ",
   #"netcfg/confirm_static=true",
   "<enter><wait>"
  ]
}

build {
  sources = [
    "source.proxmox-iso.template"
  ]
  provisioner "shell" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y ${var.vm_main_kali_package} ${var.vm_aux_kali_package}",
      "sudo usermod -aG docker ${var.vm_username}",
      "echo \"NCAE RedTeam Kali\n\nBuild date: $(date)\n\n\" | sudo tee /etc/issue",
      "mkdir ~/scripts",
      "mkdir ~/bin",
      "sudo hostnamectl hostname redteam-kali",
      "sudo sed -i 's/localhost/redteam-kali/' /etc/hosts",
      "echo \"${var.vm_user_password}\n${var.vm_user_password}\n\" | sudo passwd ${var.vm_username}",
      "echo ${var.vm_user_pub_key} >~/.ssh/redteam_key.pub",
      "echo ${var.vm_user_priv_b64_key} | base64 -d >~/.ssh/redteam_key",
      "chmod 600 ~/.ssh/redteam_key"
    ]
  }
  provisioner "file" {
    source = "./files/kali_network"
    destination = "~/interfaces"
  }
  provisioner "file" {
    source = "./files/motd"
    destination = "~/motd"
  }
  provisioner "file" {
    source = "${var.path_to_scripts_directory}"
    destination = "~/"
  }
  provisioner "shell" {
    inline = [
      "sudo mv ~/interfaces /etc/network/interfaces",
      "sudo mv ~/motd /etc/motd",
      "bash ~/scripts/setup/setup-ssh-config.sh",
      "ln -s ~/scripts/setup/netsetup.sh ~/bin/netsetup.sh"
    ]
  }
}
